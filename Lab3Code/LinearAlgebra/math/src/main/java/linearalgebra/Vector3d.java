// Christopher Bartos
// 2131542
package linearalgebra;

public class Vector3d {
    private final double x;
    private final double y;
    private final double z;

    public Vector3d (double x, double y, double z) {
        this.x = x;
        this.y = y;
        this.z = z;
    }
    public double getX() {
        return x;
    }
    public double getY() {
        return y;
    }
    public double getZ() {
        return z;
    }
    public double getMagnitude() {
        return Math.sqrt(x*x + y*y + z*z);
    }
    public double dotProduct (Vector3d vector3d) {
        return (x * vector3d.getX() + y * vector3d.getY() + z * vector3d.getZ());
    }
    public Vector3d add (Vector3d vector3d) {
        return new Vector3d(x + vector3d.getX(), y + vector3d.getY(), z + vector3d.getZ());
    }


}
