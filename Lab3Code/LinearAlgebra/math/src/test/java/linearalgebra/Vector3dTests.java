// Christopher Bartos
// 2131542

package linearalgebra;

import static org.junit.Assert.assertEquals;

import org.junit.Test;

public class Vector3dTests {
    
    @Test
    public void testVector3d() {
        Vector3d vector3d = new Vector3d(1, 2, 3);
        assertEquals(1, vector3d.getX(), 0.0);
        assertEquals(2, vector3d.getY(), 0.0);
        assertEquals(3, vector3d.getZ(), 0.0);
    }

    @Test
    public void testMagnitude() {
        Vector3d vector3d = new Vector3d(1, 2, 3);
        assertEquals(Math.sqrt(14), vector3d.getMagnitude(), 0.0);
    }

    @Test
    public void testDotProduct() {
        Vector3d vector3d1 = new Vector3d(1, 2, 3);
        Vector3d vector3d2 = new Vector3d(1, 2, 3);
        assertEquals(14, vector3d1.dotProduct(vector3d2), 0.0);
    }

    @Test
    public void testAdd() {
        Vector3d vector3d1 = new Vector3d(1, 2, 3);
        Vector3d vector3d2 = new Vector3d(1, 2, 3);
        Vector3d vector3d3 = vector3d1.add(vector3d2);
        assertEquals(2, vector3d3.getX(), 0.0);
        assertEquals(4, vector3d3.getY(), 0.0);
        assertEquals(6, vector3d3.getZ(), 0.0);
    }

}
